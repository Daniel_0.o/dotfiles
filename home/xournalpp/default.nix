{ config, lib, pkgs, ... }:
with lib;
let 
  cfg = config.home_modules.xournalpp;
in
{
  options.home_modules.xournalpp = {
    enable = mkEnableOption "Enable xournalpp";
  };
  config = mkIf cfg.enable {
    home.packages = [
      pkgs.xournalpp
    ];
    home.file = {
      ".config/xournalpp" = {
        source = ../../config/xournalpp;
        recursive = true;
      };
    };
  };
}
