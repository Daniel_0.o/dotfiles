{  username, ... }: 
{
  programs.home-manager.enable = true;
  home = {
    inherit username;
    homeDirectory = "/home/${username}";
    stateVersion = "24.05"; 
  };
  imports =
  [
    ./qtile
    ./alacritty
    ./git
    ./nvim
    ./pcmanfm
    ./rofi
    ./zathura
    ./xournalpp
    ./gtk_icons
    ./fonts
    ./firefox
    ./nix_helper
    ./vlc_video_player
    ./obsidian
    ./direnv
    ./terminal_multiplexer
    ./shells
    ./mgba
  ];
}
