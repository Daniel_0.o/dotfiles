{ config, lib, pkgs, ...}:
with lib;
let 
  cfg = config.home_modules.direnv;
  direnvPkg = pkgs.direnv;
in
{
  options.home_modules.direnv = {
    enable = mkEnableOption "Enable direnv";
    enableOnlyWithTmux = mkOption {
      type = types.bool;
      description = "Only enable direnv if inside a tmux session";
      default = false;
    };
  };
  config = mkIf cfg.enable {
    programs.direnv = {
      package = direnvPkg;
      enable = true;
      nix-direnv.enable = true;
      enableBashIntegration = !cfg.enableOnlyWithTmux;
    };
  };
}
