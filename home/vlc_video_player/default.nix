{ config, lib, pkgs, ...}:
with lib;
let 
  cfg = config.home_modules.vlc;
in
{
  options.home_modules.vlc = {
    enable = mkEnableOption "";
  };
  config = mkIf cfg.enable {
    home.packages = [
      pkgs.vlc
    ];
  };
}
