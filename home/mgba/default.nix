{ pkgs-unstable, lib, config, ... }: 
with lib; 
let 
  cfg = config.home_modules.mgba;
in
{
  options.home_modules.mgba = {
    enable = mkEnableOption "Enable mgba emulator";
  };
  config = mkIf cfg.enable {
    home.packages = [  pkgs-unstable.mgba ];
  };
}
