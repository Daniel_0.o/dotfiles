{ config, lib, pkgs-unstable, ...}:
with lib;
let 
  cfg = config.home_modules.obsidian;
in
{
  options.home_modules.obsidian = {
    enable = mkEnableOption "Enable obsidian";
  };
  config = mkIf cfg.enable {
    home.packages = [ pkgs-unstable.obsidian ];
  };
}
