{ config, pkgs, lib, ... }: 
with lib;
let 
  cfg = config.home_modules.pcmanfm;
in
{
  options.home_modules.pcmanfm = {
    enable = mkEnableOption "Enable the pcmanfm file explorer";
  };
  config = mkIf cfg.enable {
    home.packages = [
      pkgs.pcmanfm
    ];
  };
}
