{ config, lib, pkgs, ... }:
with lib;
let 
  cfg = config.home_modules.gtk-icons;
in
{
  options.home_modules.gtk-icons = {
    enable = mkEnableOption "Enable custom gtk icon";
  };
  config = mkIf cfg.enable {
    gtk.iconTheme =  {
      package = pkgs.papirus-icon-theme.override {
        color = "green";
      };
      name = "Papirus-Dark";
    };
  };
}
