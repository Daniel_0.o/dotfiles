{ config, lib, pkgs-unstable, ... }:
with lib;
let 
  cfg = config.home_modules.shells.fish;
  isStarShipEnable = config.home_modules.shells.starship_prompt.enable;
  starshipPkg = pkgs-unstable.starship;
  starshipConfig = /* fish */ ''
    ${starshipPkg}/bin/starship init fish | source
  '';
in
{
  options.home_modules.shells.fish = {
    enable = mkEnableOption "Enable fish shell";
  };
  config = mkIf cfg.enable {
    stylix.targets.fish.enable = false;
    programs.fish = {
      enable = true;
      shellAliases = {
        tmux = "tmux new-session -s $(basename $(pwd))";
        z = "zellij";
        ts = "tmux-sessionizer";
      };
      shellInit = /* fish */ ''
        set fish_greeting
      '';
      shellInitLast = 
        lib.optionalString isStarShipEnable starshipConfig;
    };
  };
}
