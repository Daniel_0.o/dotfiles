{ config, pkgs-unstable, lib, ...}:
with lib;
let 
  cfg = config.home_modules.shells.starship_prompt;
  starshipPkg = pkgs-unstable.starship;
in
{
  options.home_modules.shells.starship_prompt = {
    enable = mkEnableOption "Enable starship prompt";
  };
  config = mkIf cfg.enable { 
    programs = {
      starship = {
        package = starshipPkg;
        enable = true;
        settings = {
          add_newline = true;  
          format="$directory$git_branch$git_commit$git_state$git_metrics$git_status$nix_shell$all";
          directory = {
            style = "bold yellow";
            truncation_length = 1;
          };
          character = {
            format="$symbol";
            success_symbol = "[%](white) ";
            error_symbol = "[%](bold red) ";
            vimcmd_symbol = "[%](white) ";
            vimcmd_replace_one_symbol = "[%](bold purple) ";
            vimcmd_replace_symbol = "[%](bold purple) ";
            vimcmd_visual_symbol = "[%](bold yellow) ";
          };
          git_branch = {
            format = ''\([$branch](bold purple)'';
          };
          git_status = {
            format = ''([$all_status$ahead_behind]($style))\) '';
          };
          nix_shell = {
            format =  ''(\[[$name]($style)\]) '';
            symbol="";
            style="bold blue";
          };
        };
      };
    };
  };
}
