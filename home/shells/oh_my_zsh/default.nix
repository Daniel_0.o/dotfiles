{ config, lib, pkgs, ...}:
with lib;
let
  cfg = config.home_modules.shells.oh_my_zsh;
in
{
  options.home_modules.shells.oh_my_zsh = {
    enable = mkEnableOption "Enable Oh My Zsh";
  };
  config = mkIf cfg.enable {
    programs = {
      zsh = {
        enable = true;
        syntaxHighlighting.enable = true;
        autosuggestion.enable = true;
        enableCompletion = true;
        plugins =
        [
          ## This plugin let you use zsh as default shell while inside nix-shell
          {
            name = "zsh-nix-shell";
            file = "nix-shell.plugin.zsh";
            src = pkgs.fetchFromGitHub {
              owner = "chisui";
              repo = "zsh-nix-shell";
              rev = "v0.8.0";
              sha256 = "1lzrn0n4fxfcgg65v0qhnj7wnybybqzs4adz7xsrkgmcsr0ii8b7";
            };
          }
          {
            name = "zsh-fzf-history-search";
            file = "zsh-fzf-history-search.plugin.zsh";
            src = builtins.fetchGit {
              url = "https://github.com/joshskidmore/zsh-fzf-history-search";
              rev ="d5a9730b5b4cb0b39959f7f1044f9c52743832ba";
            };
          }
        ];
        sessionVariables = {
            ZSH_FZF_HISTORY_SEARCH_REMOVE_DUPLICATES = 1;
            ZSH_FZF_HISTORY_SEARCH_DATES_IN_SEARCH =  0;
            ZSH_FZF_HISTORY_SEARCH_EVENT_NUMBERS = 0;
        };
        oh-my-zsh = {
          enable = true;
          theme = "nixos";
          plugins = 
          [
            # Custom plug that display if i'm inside a nix shell
            "in_nix_shell"
          ];
          /*  
            Custom is the custom folder for oh-my-zsh.

            builtins.toPath reads the relative path, make a "link" of it 
            iniside nix store, and then return the path of that link as a string
          */
          custom  = builtins.toPath ../../../config/oh_my_zsh_custom;
        };
      };
    };
  };
}
