{...}:
{
  imports = 
  [
    ./oh_my_zsh 
    ./bash
    ./fish
    ./starship_prompt
  ];
}
