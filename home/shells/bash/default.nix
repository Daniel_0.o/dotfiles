{ config, lib, pkgs-unstable, pkgs,  ...}: 
with lib;
let
  cfg = config.home_modules.shells.bash;

  enableDirenvOnlyWithTmux = config.home_modules.direnv.enableOnlyWithTmux; 
  direnvPkg = pkgs.direnv;
  direnvConfig = /* bash */ ''
    if [[ -n $TMUX ]]; then
      eval "$(${direnvPkg}/bin/direnv hook bash)"
    fi
  '';

  isStarShipEnable = config.home_modules.shells.starship_prompt.enable;
  starshipPkg = pkgs-unstable.starship;
  starshipConfig = /* bash */ ''
    eval "$(${starshipPkg}/bin/starship init bash)"
  '';
in
{
  options.home_modules.shells.bash = {
    enable = mkEnableOption "Enable custom bash config";
  };
  config = mkIf cfg.enable {
    programs.fzf.enable = true;
    programs.bash = {
      enable = true;
      shellAliases = {
        t = "tmux";
        z = "zellij";
      };
      bashrcExtra = /* bash */ ''
          bind '"\C-p": "\e[A"'
          if command -v fzf-share >/dev/null; then
            source "$(fzf-share)/key-bindings.bash"
            source "$(fzf-share)/completion.bash"
          fi
      '' 
      + lib.optionalString isStarShipEnable starshipConfig
      + lib.optionalString enableDirenvOnlyWithTmux direnvConfig;
    };
  };
}
