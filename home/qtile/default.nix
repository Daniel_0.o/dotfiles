{ pkgs, lib, config, ... }: 
with lib;
let 
  cfg = config.home_modules.qtile;
in
{
  options.home_modules.qtile = { 
     enable = mkEnableOption "Enable qtile configuration files"; 
  };
  config = mkIf cfg.enable {
    ## Enable some programs that are required on my qtile config
    home.packages = with pkgs; 
    [
      scrot
      xclip
      xorg.xmodmap
    ];
    home.file.".config/qtile" = {
      source = ../../config/qtile;
      recursive = true;
    };
  };
}
