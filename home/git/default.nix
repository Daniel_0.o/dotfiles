{ config, pkgs, lib, username, ...}:
with lib;
let 
  cfg = config.home_modules.git;
  four_hours = "14400";
in
{
  options.home_modules.git = { 
    enable = mkEnableOption "enable git";
    userName = mkOption {
      default = username;
      description = ''
       The username for the git account
      '';
    };
    userEmail = mkOption {
      default = "${username}${username}.com";
      description = ''
       The email for the git account
      '';
    };
  };

  config  = mkIf cfg.enable {
    home.packages = [
      pkgs.lazygit
    ];
    programs.git = {
      enable = true;
      userName = cfg.userName;
      userEmail = cfg.userEmail;
      aliases = {
        s = "status";
        lg = "log --oneline --all --graph";
      };
      extraConfig = {
        credential = {
          helper = [
            "cache --timeout ${four_hours}"
          ];
        };
        core.askPass = "";
      };
    };
  };
}
