{ pkgs, lib, config, ... }: 
with lib;
let 
  cfg = config.home_modules.fonts;
in
{
  options.home_modules.fonts = {
    enable = mkEnableOption "Enable custom system fonts"; 
  };
  config = mkIf cfg.enable {
    fonts.fontconfig.enable = true;
    home.packages = [
      (pkgs.nerdfonts.override { 
        fonts = [ 
          "FiraCode" 
          "FiraMono"
        ]; 
      })
    ];
  };
}
