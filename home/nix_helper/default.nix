{ config, pkgs, lib, ...}:
with lib;
let 
  cfg = config.home_modules.nix_helper;
in
{
  options.home_modules.nix_helper = {
    enable = mkEnableOption "Enable nixer helper command";
  };
  config = mkIf cfg.enable {
    home.packages = [
      pkgs.nh
    ];
  };
}

