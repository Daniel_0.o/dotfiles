{ config, lib, ... }:
with lib;
let 
  cfg = config.home_modules.rofi;
in
{
  options.home_modules.rofi = { 
    enable = mkEnableOption "Enable rofi application launcher";
  };
  config = mkIf cfg.enable {
    programs.rofi = {
      enable = true;
    };
  };
}
