{ config, lib, ...}:
let 
  cfg = config.home_modules.zathura;
in
{
  options.home_modules.zathura = {
    enable = lib.mkEnableOption "Enable Zathura pdf viewer";
  };
  config = lib.mkIf cfg.enable {
    programs.zathura = {
      enable = true;
      options = {
        selection-clipboard = "clipboard";
        highlight-fg = lib.mkForce "#fb6150";
        highlight-color = lib.mkForce "rgba(22,22,22,0.6)";
      };
      mappings = {
        u = "scroll half-up";
        e = "scroll half-down";
      };
    };
  };
}
