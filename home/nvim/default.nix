{ config, pkgs, lib, ...}:
with lib;
let 
  cfg = config.home_modules.nvim;
in
{
  options.home_modules.nvim = {
    enable = mkEnableOption "Enable neovim text editor";
  };
  config = mkIf cfg.enable {
    programs.neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      ## Dependencies for my neovim config
      extraPackages = with pkgs; [
        # Telescope
        fd
        fzf
        ripgrep
        # Tresitter
        gcc
        # Lsp 
        sumneko-lua-language-server
        nil
      ];
    };
    home = {
      file.".config/nvim" = {
        source = ../../config/nvim;
        recursive = true;
      };
    };
  };
}
