{ pkgs-unstable, lib, config, ... }: 
with lib;
let 
  cfg = config.home_modules.alacritty;
in
{
  options.home_modules.alacritty = {
    enable = mkEnableOption "Enable alacritty terminal emulator"; 
  };
  config = mkIf cfg.enable {
    programs.alacritty = {
      package = pkgs-unstable.alacritty;
      enable = true;
      settings = mkForce { ## mkForce is required here, otherwise stylix will override the configs
        colors = { 
          draw_bold_text_with_bright_colors = false;
          primary = {
             background = "0x080808";
             foreground = "0xd5c4a1";
           };
           normal = {
             black = "0x161616";
             red = "0xfb6150";
             green = "0xa7a923";
             yellow = "0xfabd2f";
             blue = "0x74b4b4";
             magenta = "0x70c653";
             cyan = "0xfa862e";
             white = "0xfbf1c7";
           };
           bright = {
             black = "0x2d2a29";
             red = "0xef6c8f";
             green = "0xa7a923";
             yellow = "0xfabd2f";
             blue = "0x74b4b4";
             magenta = "0x70c653";
             cyan = "0xfa862e";
             white = "0xfbf1c7";
          };
        };
        env = {
          TERM = "xterm-256color";
        };
        font = {
          size = 18;
          normal = {
            family = "FiraMono Nerd Font Mono";
            style = "medium";
          };
          bold.family = "FiraMono Nerd Font Mono";
        };
        window = {
          opacity=1;
        };
      };
    };
  };
}
