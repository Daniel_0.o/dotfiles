{ config, lib, pkgs, ...}:
with lib;
let 
  cfg = config.home_modules.terminal_multiplexer.tmux;
  tmux = pkgs.tmux;
  fzf = pkgs.fzf;
  direnv = pkgs.direnv;
  tmux-sessionizer = pkgs.writeShellScriptBin "tmux-sessionizer" ''
    # Original script from https://github.com/ThePrimeagen/.dotfiles/blob/master/bin/.local/scripts/tmux-sessionizer
    if [[ $# -eq 1 ]]; then
        selected=$1
    else
        selected=$(find ~/Projects/ -mindepth 2 -maxdepth 2  -type d | ${fzf}/bin/fzf)
    fi
    if [[ -z $selected ]]; then
        exit 0
    fi
    cd $selected
    selected_name=$(basename "$selected" | tr . _)
    if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
        if [ -f ".envrc" ]; then
            ${direnv}/bin/direnv exec $selected ${tmux}/bin/tmux new-session -s $selected_name -c $selected
        else
            ${tmux}/bin/tmux new-session -s $selected_name -c $selected
        fi
    exit 0
    fi
    if ! ${tmux}/bin/tmux has-session -t=$selected_name 2> /dev/null; then
        if [ -f .envrc ]; then 
            ${direnv}/bin/direnv exec $selected  ${tmux}/bin/tmux  new-session -ds $selected_name -c $selected
        else
            ${tmux}/bin/tmux  new-session -ds $selected_name -c $selected
        fi
    fi
    ${tmux}/bin/tmux switch-client -t $selected_name
  '';
in
{
  options.home_modules.terminal_multiplexer.tmux = {
    enable = mkEnableOption "Enable Tmux";
  };
  config = mkIf cfg.enable {
    home.packages = [ tmux-sessionizer ];
    programs.tmux = {
      enable = true;
      extraConfig = /* tmux */ ''
        set -g default-terminal "screen-256color"
        set -g default-terminal "tmux-256color"
        set -ag terminal-overrides ",xterm-256color:RGB"
        set -g prefix C-a 
        unbind C-b 
        bind-key C-a send-prefix
        unbind % 
        bind v split-window -h 
        unbind '"'
        bind - split-window  -v
        unbind r 
        bind r source-file ~/.config/tmux/tmux.conf
        bind -r j resize-pane -D 5 
        bind -r k resize-pane -U 5 
        bind -r h resize-pane -L 5 
        bind -r l resize-pane -R 5 
        set -g mouse on 
        bind -r m resize-pane -Z 
        set-window-option -g mode-keys vi
        bind-key -T copy-mode-vi 'v' send -X begin-selection # start selecting text with "v"
        bind-key -T copy-mode-vi 'y' send -X copy-selection # copy text with "y"
        unbind -T copy-mode-vi MouseDragEnd1Pane # don't exit copy mode when dragging with mouse
        set -g status-bg black
        set -g status-fg white
        set -g pane-border-style fg=#121212
        set -g pane-active-border-style "bg=default fg=#121212"
        set -sg escape-time 10
        set -g update-environment "PATH PWD DIRENV_DIFF DIRENV_DIR DIRENV_FILE DIRENV_WATCHES IN_NIX_SHELL name"
      '';
    };
  };
}
