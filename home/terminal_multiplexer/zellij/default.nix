{ config, lib, pkgs-unstable, ... }:
with lib;
let 
  cfg = config.home_modules.terminal_multiplexer.zellij;
in
{
  options.home_modules.terminal_multiplexer.zellij = {
    enable = mkEnableOption "Enable zellij terminal multiplexer";
  };
  config = mkIf cfg.enable {
    stylix.targets.zellij.enable = false;
    programs.zellij = {
      enable = true;
      package = pkgs-unstable.zellij;
    };
    home.file.".config/zellij" = {
      source = ../../../config/zellij;
      recursive = true;
    };
  };
}
