IN_NIX_SHELL_PREFIX=''
IN_NIX_SHELL_SUFFIX=''
function in_nix_shell() 
{
    if [[ -n "$IN_NIX_SHELL" ]]; then
        echo "${IN_NIX_SHELL_PREFIX}nix-shell${IN_NIX_SHELL_SUFFIX}";
    fi
} 
