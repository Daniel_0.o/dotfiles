IN_NIX_SHELL_PREFIX='['
IN_NIX_SHELL_SUFFIX='] '

GIT_INFO='$(git_prompt_info)'
PROMPT="%{$fg[red]%}%m%{$reset_color%}:%{$fg[green]%}%c \
%{$fg[blue]%}$GIT_INFO%{$fg[cyan]%}$(in_nix_shell)%{$reset_color%}%# "

ZSH_THEME_GIT_PROMPT_PREFIX='('
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$blue%}) "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[yellow]%}%1 %{✗%}%{$fg[blue]%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
