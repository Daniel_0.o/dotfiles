{
  description = "My flake configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-24.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    stylix = { 
      url = "github:danth/stylix/release-24.05"; 
    };

  };

  outputs = { 
    self,
    nixpkgs,
    nixpkgs-unstable,
    home-manager,
    firefox-addons,
    ... 
  }@inputs: 
  let 
    system = "x86_64-linux";
    ## Default username for using independently of the host
    username = "daniel";
    pkgs-unstable = 
      import nixpkgs-unstable { 
        inherit system; 
        config.allowUnfreePredicate = 
          pkg: builtins.elem (nixpkgs.lib.getName pkg) [ "obsidian" ];
      };
  in
  {
    nixosConfigurations.chrono = nixpkgs.lib.nixosSystem {
      specialArgs = { 
        inherit username;
        hostname = "chrono";
      };
      modules = [
        ./hosts/chrono/configuration.nix

        home-manager.nixosModules.home-manager 
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.${username} = 
                import ./hosts/chrono/home.nix;
            extraSpecialArgs = {
              inherit username;
              inherit pkgs-unstable;
              inherit firefox-addons;
            };
          };
        }

        inputs.stylix.nixosModules.stylix
        ./stylix
      ];
    };
  };
}
