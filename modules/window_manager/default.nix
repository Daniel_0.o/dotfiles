{  lib, ... }: 
{
  options.modules.windowManager = {
    enable = lib.mkEnableOption "Enable some window managers";
  };
  imports =  [
    ./qtile
  ];
}

