{ config, lib, ... }: 
with lib;
let 
  cfg = config.modules.windowManager;
in 
{
  options.modules.windowManager.qtile = {
    enable = mkEnableOption "Enable qtile as the default WM";
  };
  config = mkIf (cfg.enable && cfg.qtile.enable) {
    services = {
      xserver.windowManager.qtile = {
        enable = true;
          extraPackages = python3Packages: with python3Packages; [
            qtile-extras
          ];
      };
      ## Also enable picom for avoiding screen tearing on X11
      picom = {
        enable = true; 
        settings = {
          backend = "glx";
          vsync = true;
          unredir-if-possible = false;
        };
      };
    };
  };
}
