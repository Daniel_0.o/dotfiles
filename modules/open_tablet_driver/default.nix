{ config, lib, ... }: 
with lib;
let 
  cfg = config.modules.open-tablet-driver;
in 
{
  options.modules.open-tablet-driver = {
    enable = mkEnableOption "Enable open-tablet-driver";
  };
  config = mkIf cfg.enable {
    hardware.opentabletdriver = {
      enable = true; 
      daemon.enable = true;
    };
  };
}
