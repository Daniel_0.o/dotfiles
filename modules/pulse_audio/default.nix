{ config, lib, pkgs, ... }: 
with lib;
let 
  cfg = config.modules.pulseaudio;
in 
{
  options.modules.pulseaudio = {
    enable = mkEnableOption "Enable pulseaudio as audio controler";
  };
  config = mkIf cfg.enable {
      ## Pulsemixer is a better way to handle volume control 
      environment.systemPackages = [
        pkgs.pulsemixer
      ];
      security.rtkit.enable = true;
      services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
      };
  };
}
