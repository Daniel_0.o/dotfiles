{ config, lib, ...}:
let 
  cfg = config.modules.lightdm;
in
{
  options.modules.lightdm = {
    enable = lib.mkEnableOption "Enable lightdm as default display manager";
  }; 
  config = lib.mkIf cfg.enable {
    services.xserver.displayManager.lightdm = {
      enable = true; 
      greeters.gtk = {
      extraConfig = ''
          user-background = false
        '';
      };
    };
  };
}
