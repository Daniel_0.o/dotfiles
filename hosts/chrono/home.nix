{...}:
{
  imports = [ ../../home ];
  config.home_modules = {
    qtile.enable = true;
    alacritty.enable = true;
    nvim.enable = true;
    pcmanfm.enable = true;
    rofi.enable = true;
    zathura.enable = true;
    xournalpp.enable = true;
    gtk-icons.enable = true; 
    fonts.enable = true;
    firefox.enable = true;
    nix_helper.enable = true;
    terminal_multiplexer = { 
      zellij.enable = true; 
      tmux.enable = true; 
    };
    shells = {
      starship_prompt.enable = true;
      oh_my_zsh.enable = false;
      bash.enable = true;
      fish.enable = true;
    };
    direnv = { 
      enable = true; 
      enableOnlyWithTmux = true;
    };
    mgba.enable = true;
    vlc.enable = true;
    obsidian.enable = false;
    git = {
      enable = true;
      userName = "CarlosDanielMaturano";
      userEmail = "carlosdanielmaturano@proton.me";
    };
  };
}
