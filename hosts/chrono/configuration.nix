{ pkgs, username, ...}:
let
  defaultShell = "fish";
in
{
  imports = [
    ../../modules
    ../. # Imports common configuration for the of the hosts
    ./hardware-configuration.nix
  ]; 
  ## Because i'm using system modules, config needs to be at the toplevel
  config = {
    programs.${defaultShell}.enable = true;
    users.users.${username} = {
      isNormalUser = true;
      extraGroups = ["networkmanager" "wheel"];
      initialPassword = "123";
      shell = pkgs.${defaultShell};
    };
    modules = {
      pulseaudio.enable = true;
      lightdm.enable = true;
      windowManager = {
        enable = true;
        qtile.enable = true;
      };
      open-tablet-driver.enable = true;
    };
  };
}
