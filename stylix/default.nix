{ pkgs, ... }:
{
  stylix = {
    enable = true;
    autoEnable = true;
    base16Scheme = {
      base00 = "080808"; 
      base01 = "161616"; 
      base02 = "2d2a29"; 
      base03 = "665c54"; 
      base04 = "bdae93"; 
      base05 = "d5c4a1"; 
      base06 = "ebdbb3"; 
      base07 = "fbf1c7"; 
      base08 = "fb6150"; 
      base09 = "ef6c8f"; 
      base0A = "fabd2f"; 
      base0B = "a7a923"; 
      base0C = "74b594"; 
      base0D = "74b4b4"; 
      base0E = "70c653"; 
      base0F = "fa862e"; 
    };
    image = ./wallpaper.jpg;
    cursor = {
      package = pkgs.bibata-cursors;
      size = 20;
      name = "Bibata-Modern-Ice";
    };
    fonts = {
      monospace = {
        package = pkgs.nerdfonts.override {
          fonts = [
            "FiraMono"
            "FiraCode"
          ];
        };
        name = "FiraMono Nerd Font Mono";
      };
      sansSerif = {
        package = pkgs.open-sans;
        name = "Open Sans";
      };
      serif = {
        package = pkgs.vegur;
        name = "Vegur";
      };
    };
    targets = {
      grub.enable = true;
      lightdm.enable = true;
      gtk.enable = true;
      fish.enable = false;
    };
  };
}
